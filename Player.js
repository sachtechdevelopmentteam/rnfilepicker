import React from 'react'
import { View, Image, Slider, TouchableOpacity, Platform, Alert} from 'react-native';
import {
    Container,
    Content,
    Header,
    Left,
    Right,
    Body,
    Text,
    Icon,
    Footer
    } from 'native-base'

import Sound from 'react-native-sound';
import {responsiveFontSize,
        responsiveHeight,
        responsiveWidth} from 'react-native-responsive-dimensions';
const img_speaker = require('./resources/ui_speaker.png');
const img_pause = require('./resources/ui_pause.png');
const img_play = require('./resources/ui_play.png');
const img_playjumpleft = require('./resources/ui_playjumpleft.png');
const img_playjumpright = require('./resources/ui_playjumpright.png');

export default class PlayerScreen extends React.Component{

    static navigationOptions = props => ({
        title:props.navigation.state.params.title,
    })

    constructor(){
        super();
        this.state = {
            playState:'paused', //playing, paused
            playSeconds:0,
            duration:0
        }
        this.sliderEditing = false;
    }

    componentDidMount(){
        this.play();
        
        this.timeout = setInterval(() => {
            if(this.sound && this.sound.isLoaded() && this.state.playState == 'playing' && !this.sliderEditing){
                this.sound.getCurrentTime((seconds, isPlaying) => {
                    this.setState({playSeconds:seconds});
                })
            }
        }, 100);
    }
    componentWillUnmount(){
        if(this.sound){
            this.sound.release();
            this.sound = null;
        }
        if(this.timeout){
            clearInterval(this.timeout);
        }
    }

    onSliderEditStart = () => {
        this.sliderEditing = true;
    }
    onSliderEditEnd = () => {
        this.sliderEditing = false;
    }
    onSliderEditing = value => {
        if(this.sound){
            this.sound.setCurrentTime(value);
            this.setState({playSeconds:value});
        }
    }

    play = async () => {
        if(this.sound){
            this.sound.play(this.playComplete);
            this.setState({playState:'playing'});
        }else{
            const filepath = this.props.navigation.state.params.filepath;
            console.log('[Play]', filepath);
    
            this.sound = new Sound(filepath, '', (error) => {
                if (error) {
                    console.log('failed to load the sound', error);
                    Alert.alert('Notice', 'audio file error. (Error code : 1)');
                    this.setState({playState:'paused'});
                }else{
                    this.setState({playState:'playing', duration:this.sound.getDuration()});
                    this.sound.play(this.playComplete);
                }
            });    
        }
    }
    playComplete = (success) => {
        if(this.sound){
            if (success) {
                console.log('successfully finished playing');
            } else {
                console.log('playback failed due to audio decoding errors');
                Alert.alert('Notice', 'audio file error. (Error code : 2)');
            }
            this.setState({playState:'paused', playSeconds:0});
            this.sound.setCurrentTime(0);
        }
    }

    pause = () => {
        if(this.sound){
            this.sound.pause();
        }

        this.setState({playState:'paused'});
    }

    jumpPrev15Seconds = () => {this.jumpSeconds(-15);}
    jumpNext15Seconds = () => {this.jumpSeconds(15);}
    jumpSeconds = (secsDelta) => {
        if(this.sound){
            this.sound.getCurrentTime((secs, isPlaying) => {
                let nextSecs = secs + secsDelta;
                if(nextSecs < 0) nextSecs = 0;
                else if(nextSecs > this.state.duration) nextSecs = this.state.duration;
                this.sound.setCurrentTime(nextSecs);
                this.setState({playSeconds:nextSecs});
            })
        }
    }

    getAudioTimeString(seconds){
        const h = parseInt(seconds/(60*60));
        const m = parseInt(seconds%(60*60)/60);
        const s = parseInt(seconds%60);

        return ((h<10?'0'+h:h) + ':' + (m<10?'0'+m:m) + ':' + (s<10?'0'+s:s));
    }

    render(){

        const currentTimeString = this.getAudioTimeString(this.state.playSeconds);
        const durationString = this.getAudioTimeString(this.state.duration);

        return (
            <Container style={{backgroundColor:'black'}}>
                <Header style={{backgroundColor:'#302F34'}}>
                    <Body style={{alignItems:'center'}}>
                        <Text style={{fontSize:responsiveFontSize(2.3),
                                        color:'white'}}>
                            {this.props.navigation.state.params.title}
                        </Text>
                    </Body>
                </Header>
                <Content contentContainerStyle={{flex:1,alignItems:'center'}}>
                        <Image source={require('./assets/music_logo.png')}
                            resizeMode={'cover'}
                            style={{
                                width:responsiveWidth(45),
                                height:responsiveHeight(40),
                                marginTop:responsiveHeight(10)
                            }}
                            />

                <View style={{marginVertical:responsiveHeight(3), 
                            marginHorizontal:responsiveWidth(5),
                            marginTop:responsiveHeight(20),
                             flexDirection:'row'}}>
                    <Text style={{color:'#726F5F', 
                                alignSelf:'center',
                                fontSize:responsiveFontSize(1.5)}}>{currentTimeString}</Text>
                    <Slider
                        onTouchStart={this.onSliderEditStart}
                        // onTouchMove={() => console.log('onTouchMove')}
                        onTouchEnd={this.onSliderEditEnd}
                        // onTouchEndCapture={() => console.log('onTouchEndCapture')}
                        // onTouchCancel={() => console.log('onTouchCancel')}
                        onValueChange={this.onSliderEditing}
                        value={this.state.playSeconds} 
                        maximumValue={this.state.duration} 
                        maximumTrackTintColor='#5D5A51' 
                        minimumTrackTintColor='#726F5F' 
                        thumbTintColor='#726F5F' 
                        style={{flex:1, 
                                alignSelf:'center',
                                maximumTrackTintColor:'#726F5F',
                                marginHorizontal:Platform.select({ios:5})}}/>
                    <Text style={{color:'#726F5F',
                                 alignSelf:'center',
                                 fontSize:responsiveFontSize(1.5)}}>{durationString}</Text>
                </View>

                <View style={{flexDirection:'row',
                             justifyContent:'center',
                             alignItems:'center',
                             }}>
                    <TouchableOpacity onPress={this.jumpPrev15Seconds} 
                                    style={{width:responsiveWidth(20),
                                            alignItems:'center'}}>
                            <Image source={img_playjumpleft} 
                                    style={{width:responsiveWidth(8), height:responsiveHeight(5),
                                             }}
                                    resizeMode={'cover'} />
                                     
                    </TouchableOpacity>

                    {this.state.playState == 'playing' && 
                    <TouchableOpacity onPress={this.pause} 
                        style={{width:responsiveWidth(20),alignItems:'center'}}>
                        <Image source={img_pause} style={{width:responsiveWidth(10), height:responsiveHeight(4)}}/>
                    </TouchableOpacity>}
                    {this.state.playState == 'paused' && 
                    <TouchableOpacity onPress={this.play} 
                            style={{width:responsiveWidth(20),
                                    alignItems:'center'}}>
                        <Image source={img_play} style={{width:responsiveWidth(8), height:responsiveHeight(4)}}
                           resizeMode={'cover'} />
                    </TouchableOpacity>}
                    <TouchableOpacity onPress={this.jumpNext15Seconds}
                         style={{width:responsiveWidth(20),
                                alignItems:'center'}}>
                        <Image source={img_playjumpright} 
                           style={{width:responsiveWidth(8), 
                                    height:responsiveHeight(5),
                                  }}
                           resizeMode={'cover'} />
                       
                    </TouchableOpacity>
                </View>
                </Content>
            </Container>
            
        )
    }
}
