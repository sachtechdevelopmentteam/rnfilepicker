import React from 'react';
import { createStackNavigator, createAppContainer ,StackNavigator} from "react-navigation";

import App from './App';
import Player from './Player';
// import PlayerScreen from 'react-native-sound-playerview'


  const AppNavigator = createStackNavigator(
      {
        App: App,
        Player: Player
      },
      {
        initialRouteName: "App",
        headerMode:'none'
      }
  );


  export default createAppContainer(AppNavigator);
