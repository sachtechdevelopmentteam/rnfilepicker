/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text,Image, View,TouchableOpacity,ToastAndroid} from 'react-native';
import { DocumentPicker, DocumentPickerUtil } from 'react-native-document-picker';
import { createStackNavigator, createAppContainer } from 'react-navigation';
var SoundPlayer = require('react-native-sound');
import FileSystem from 'react-native-filesystem';

var song = null;


const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});


export default class App extends Component{
  constructor(props){
    super(props);
    this.state={
      uri:'',
      pause:false
    }
  }


  onPlayBtn(){
    song = new SoundPlayer('jz.mp3',SoundPlayer.MAIN_BUNDLE, (error) =>{
      if(error){
          alert(JSON.stringify(error));
      }
      else{
        if(song != null){
          this.props.navigation.navigate('Player', {title:'Test', filepath:'jz.mp3'});
          song.play((success)=>{
              if(!success){
                alert('playing error');
              }
          })
        }
      }
    });
  }

  onPause(){
    if(song != null){
      song.play((success)=>{
          if(!success){
            alert('playing error pause btn');
          }
      })
  }
  else song.pause;
  this.setState({pause:!this.state.pause})
}


  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={()=>this.openFile()}>
            <Text style={styles.welcome}>Open File Picker</Text>
        </TouchableOpacity>
          {/* {(this.state.uri != '') && 
          <Image 
            source={{uri:this.state.uri}}
            resizeMode={'center'}
            style={{width:'30%',height:'30%'}}
          />
          } */}
          <TouchableOpacity onPress={()=>this.onPlayBtn()}>
            <Text style={styles.welcome}>
              Play Music
            </Text>
          </TouchableOpacity>
      </View>
    );
  }

  


   openFile(){
    DocumentPicker.show({
      filetype: [DocumentPickerUtil.allFiles()],
    },(error,res) => {
      alert(JSON.stringify(res));
        FileSystem.readFile(res.uri)
        .then((result)=>{
          alert( 'read from file:' + JSON.stringify(result));
        })
        .catch((err)=>{
          alert(err);
        })
      this.setState({uri:res.uri})
    });
 
    
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
